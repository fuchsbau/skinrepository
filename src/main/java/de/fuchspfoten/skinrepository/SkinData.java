package de.fuchspfoten.skinrepository;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents data for a single skin.
 */
@RequiredArgsConstructor
@ToString
public class SkinData implements ConfigurationSerializable {

    /**
     * The value of the skin data, i.e. the signed texture element.
     */
    private @Getter final String value;

    /**
     * The signature of the texture element, provided by Mojang.
     */
    private @Getter final String signature;

    /**
     * Deserialization constructor.
     *
     * @param source The source map.
     */
    public SkinData(final Map<String, Object> source) {
        value = (String) source.get("value");
        signature = (String) source.get("signature");
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("value", value);
        result.put("signature", signature);
        return result;
    }
}
