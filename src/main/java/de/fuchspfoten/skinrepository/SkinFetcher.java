package de.fuchspfoten.skinrepository;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Fetches skins via the mineskin API.
 */
@RequiredArgsConstructor
public class SkinFetcher implements Runnable {

    /**
     * The sender to notify when data is fetched.
     */
    private final CommandSender sender;

    /**
     * The URL in string form at which the image resides.
     */
    private final String imageURL;

    /**
     * The key to save the skin to.
     */
    private final String key;

    @Override
    public void run() {
        SkinRepositoryPlugin.getSelf().runSync(() -> {
            sender.sendMessage("Beginning to fetch");
        });

        try {
            final URL restURL = new URL("https://api.mineskin.org/generate/url");
            final HttpURLConnection connection = (HttpURLConnection) restURL.openConnection();
            connection.setReadTimeout(60000);
            connection.setConnectTimeout(60000);
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            try (final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(),
                    StandardCharsets.UTF_8))) {
                final String postData = String.format("url=%1$s", URLEncoder.encode(imageURL, "UTF-8"));
                writer.write(postData);
                writer.flush();
            }
            connection.connect();

            final String requestData;
            try (final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                final StringBuilder buffer = new StringBuilder();
                while (true) {
                    final String line = reader.readLine();
                    if (line == null) {
                        break;
                    }
                    buffer.append(line);
                }
                requestData = buffer.toString().trim();
            }
            connection.disconnect();

            final JsonObject result = new JsonParser().parse(requestData).getAsJsonObject();
            final JsonObject textures = result.get("data").getAsJsonObject().get("texture").getAsJsonObject();
            final SkinData skinData = new SkinData(textures.get("value").getAsString(),
                    textures.get("signature").getAsString());
            SkinRepositoryPlugin.getSelf().runSync(() -> {
                Bukkit.getLogger().info("Got SkinData: " + skinData);
                SkinRepositoryPlugin.getSelf().addSkin(key, skinData);
                sender.sendMessage("Completed!");
            });
        } catch (final IOException ex) {
            SkinRepositoryPlugin.getSelf().runSync(() -> {
                sender.sendMessage("Fetching from URL failed");
                ex.printStackTrace();
            });
        }
    }
}
