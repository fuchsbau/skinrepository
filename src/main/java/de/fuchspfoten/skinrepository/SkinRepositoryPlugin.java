package de.fuchspfoten.skinrepository;

import lombok.Getter;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The main plugin class.
 */
public class SkinRepositoryPlugin extends JavaPlugin implements CommandExecutor {

    /**
     * The singleton plugin instance.
     */
    private @Getter static SkinRepositoryPlugin self;

    /**
     * Fetches skin data for the given key.
     *
     * @param key The key.
     * @return The skin data.
     */
    public SkinData getSkinData(final String key) {
        final String fullKey = String.format("skins.%1$s", key);
        if (getConfig().isSet(fullKey)) {
            return getConfig().getSerializable(fullKey, SkinData.class);
        }
        return null;
    }

    /**
     * Adds a skin for the given key.
     *
     * @param key  The key to store at.
     * @param data The data to store.
     */
    public void addSkin(final String key, final SkinData data) {
        final String fullKey = String.format("skins.%1$s", key);
        getConfig().set(fullKey, data);
        saveConfig();
    }

    /**
     * Runs a task synchronously.
     *
     * @param task The task.
     */
    public void runSync(final Runnable task) {
        getServer().getScheduler().scheduleSyncDelayedTask(this, task);
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label,
                             final String[] args) {
        if (!(sender instanceof ConsoleCommandSender)) {
            sender.sendMessage("Console-Only.");
            return true;
        }

        if (args.length != 1) {
            sender.sendMessage("Syntax: /skinrepo <key>");
            return true;
        }

        final String sourceUrl = String.format(getConfig().getString("repoImageUrl"), args[0]);
        getServer().getScheduler().runTaskAsynchronously(this, new SkinFetcher(sender, sourceUrl, args[0]));
        return true;
    }

    @Override
    public void onEnable() {
        self = this;

        // Register serializable classes.
        ConfigurationSerialization.registerClass(SkinData.class);

        // Create the default configuration.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Register command executor modules.
        getCommand("skinrepo").setExecutor(this);
    }
}
